# Configurable Form in Node.JS

This is a simple web app running in NodeJS to display a form and write the responses to a Directus collection.

By "simple", we mean it is *NOT* a full-blown forms solution, and is not meant to be.  This gets the job done simply, easily, without installing an elephant to pull what a mouse can do by itself.

A second page allows the viewing of responses as a web page, which can be printed using standard browser functionality to paper or PDF.

A key feature of the app is that key form elements can be set in the app's `.env` file or `config.json` 
- DIRECTUS_TOKEN
- DIRECTUS_COLLECTION_URL
- YOUR FORM FIELDS !! (configured in config.json)
- VIEW_PAGE_USERNAME
- VIEW_PAGE_PASSWORD
- PAGE_TITLE
- HEADER_TITLE
- SUBHEADER_1
- SUBHEADER_2
- FOOTER_BACKGROUND_COLOR
- FOOTER_TEXT_COLOR
- LOGO (replace default in /app/data/public)

Just edit `/app/data/.env` and `/app/data/public/config.json` AND RESTART THE APP.

The form is deliberately not highly styled.
Minimalist and functional, not whizz-bang.
Want whizz-bang ?  Make visual improvements in `index.html`.

The view page is password-protected - change the values in `/app/data.env`.
The view page is also deliberately very plain design, so that it can be printed to paper or PDF.

**ToDo** :
- add editor for config.json

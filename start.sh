#!/bin/bash

# Path to the .env file
ENV_PATH="/app/data/.env"

# Check if the .env file exists
if [ ! -f "$ENV_PATH" ]; then
    # Copy .env from /tmp to /app/data
    cp /tmp/.env /app/data/

    # Ensure the public directory exists and copy index.html to /app/data/public
    mkdir -p /app/data/public
    mkdir -p /app/data/private
    cp /tmp/index.html /app/data/public/
    cp /tmp/view.html /app/data/private/
    cp /tmp/logo.png /app/data/public/
    cp /tmp/config.json /app/data/public/
    cp /tmp/favicon.ico /app/data/public/
fi

chown -R cloudron:cloudron /app/data

# Start the Node.js application
node /app/code/server.js

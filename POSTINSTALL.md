# Post Installlation Steps

## Configuration

1.  Copy `.env-sample` to `.env` in `/app/data`

2.  Edit `/app/data/.env` to add/set :
- your bearer token
- your Directus URL and `collection` value
- the view page username
- the view page password
- PAGE_TITLE
- HEADER_TITLE
- SUBHEADER_1
- SUBHEADER_2
- FOOTER_BACKGROUND_COLOR (optional)
- FOOTER_TEXT_COLOR (optional)

3. Edit `/app/data/public/config.json` to configure the fields set in your Directus collection.

RESTART THE APP AFTER CHANGING ANY .ENV VALUES !

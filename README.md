# custom-form-cloudron

This repo is a custom app - it is not built from another repo.

**PLEASE READ POSTINSTALL.md.**

## Configurable Form in Node.JS

This is a simple web app running in NodeJS to display a form and write the responses to a `Directus collection`.

By "simple", we mean it is *NOT* a full-blown forms solution, and is not meant to be.  This gets the job done simply, easily, without installing an elephant to pull what a mouse can do by itself.

A second page allows the viewing of responses as a web page, which can be printed using standard browser functionality to paper or PDF.

A key feature of the app is that key form elements can be set in the app's .env file or config.json.
- DIRECTUS_TOKEN
- DIRECTUS_COLLECTION_URL
- YOUR FORM FIELDS !! (edit in /app/data/public/config.json)
- VIEW_PAGE_USERNAME
- VIEW_PAGE_PASSWORD
- PAGE_TITLE
- HEADER_TITLE
- SUBHEADER_1
- SUBHEADER_2
- FOOTER_BACKGROUND_COLOR
- FOOTER_TEXT_COLOR

Just edit `/app/data/.env` and `/app/data/public/config.json` AND RESTART THE APP.

The form is deliberately not highly styled.
Minimalist and functional, not whizz-bang.
Want whizz-bang ?  Make visual style and UI changes in `index.html`.

The view page is password-protected - change the values in `/app/data.env`.
The view page is also very plain design, so that it can be printed to paper or PDF.

**ToDo** :
- add editor for config.json 

## building for cloudron

* download / clone repo to a folder on your local machine
* First Time app packagers :
    * ensure you have cloudron cli installed AND are logged in
    * ensure you have Docker installed
    * ensure you have a Docker repo AND are signed in
* use the provided script cld.sh <reponame> to build & push the repo and then install it on your Cloudron
* you will be prompted for Location: : enter where the app will be hosted, e.g. app.domain.tld


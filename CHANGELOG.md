# Initial version 

# V2
- added support for date, boolean, decimal and integer fields
- changed View Form Data link to generic wording
- removed hard-coded sort order for specific use case

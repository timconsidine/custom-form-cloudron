FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

WORKDIR /app/code

COPY package*.json ./

RUN npm install

COPY server.js start.sh ./

COPY .env index.html view.html logo.png config.json favicon.ico /tmp/

RUN chmod +x start.sh

EXPOSE 3000

# Start the application using start.sh
CMD ["/app/code/start.sh"]

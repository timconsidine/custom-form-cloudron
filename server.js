const express = require('express');
const basicAuth = require('basic-auth');
const bodyParser = require('body-parser');
const fetch = require('node-fetch');
const path = require('path');
require('dotenv').config({ path: path.resolve(__dirname, '../data/.env') });

const app = express();
const PORT = process.env.PORT || 3000;

// Body parser middleware should come before route declarations
app.use(bodyParser.json());

const USERNAME = process.env.VIEW_PAGE_USERNAME;
const PASSWORD = process.env.VIEW_PAGE_PASSWORD;

// Basic Authentication middleware
const auth = (req, res, next) => {
    const user = basicAuth(req);

    if (user && user.name === USERNAME && user.pass === PASSWORD) {
        next();
    } else {
        res.setHeader('WWW-Authenticate', 'Basic realm="401"');
        res.status(401).send('Authentication required.');
    }
};

// Assuming view.html is located in a 'private' directory
const privatePath = path.join(__dirname, '../data/private');

app.get('/config/style', (req, res) => {
    res.json({
        footerBackgroundColor: process.env.FOOTER_BACKGROUND_COLOR || '#f2f2f2',
        footerTextColor: process.env.FOOTER_TEXT_COLOR || '#000000'
    });
});

app.get('/config/texts', (req, res) => {
    res.json({
        pageTitle: process.env.PAGE_TITLE,
        headerTitle: process.env.HEADER_TITLE,
        subheader1: process.env.SUBHEADER_1,
        subheader2: process.env.SUBHEADER_2
    });
});

// Protected route for view.html
app.get('/view', auth, (req, res) => {
    res.sendFile(path.join(privatePath, 'view.html'));
});

// Setup the path for static files
const publicPath = path.join(__dirname, '../data/public');

// Serve static files from the 'public' folder
// This should come after specific routes that need authentication
app.use(express.static(publicPath));

// Directus Configurations
const directusToken = process.env.DIRECTUS_TOKEN;
const directusCollectionUrl = process.env.DIRECTUS_COLLECTION_URL;

// Route for submitting form data
app.post('/submit', async (req, res) => {
    try {
        const response = await fetch(directusCollectionUrl, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${directusToken}`
            },
            body: JSON.stringify(req.body)
        });

        if (response.ok) {
            res.status(200).json({ message: 'Data submitted successfully!', success: true });
        } else {
            res.status(500).json({ message: 'Submission failed!', success: false });
        }
    } catch (error) {
        console.error('Error:', error);
        res.status(500).json({ message: 'Error in submission', success: false });
    }
});

// Route to fetch data from Directus
app.get('/api/data', async (req, res) => {
    try {
        const directusResponse = await fetch(`${directusCollectionUrl}`, {
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${directusToken}`
            }
        });

        if (directusResponse.ok) {
            const data = await directusResponse.json();
            res.status(200).json(data);
        } else {
            res.status(500).send('Failed to fetch data from Directus');
        }
    } catch (error) {
        console.error('Error:', error);
        res.status(500).send('Server error');
    }
});

// Add a root route for health check
app.get('/', (req, res) => {
    res.status(200).send('OK');
});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
